Animes = new Mongo.Collection("animes");

var Schemas = {};

Schemas.Anime = new SimpleSchema({
  identifier: {
    type: String,
    index: true,
    unique: true,
    autoValue: function() {
      var content = this.field("title");
      if (content.isSet) {
        return content.value.replace(/(\W| )/g, "").toLowerCase();
      } else {
        this.unset(); // Prevent user from supplying her own value
      }
    }
  },
  title: {
    type: String,
    label: "Title",
    max: 200,
    min: 2
  },
  summary: {
    type: String,
    label: "Brief summary"
  },
  year: {
    type: Number,
    label: "Year at which this anime was aired",
    min: 1900,
    max: new Date().getFullYear()
  },
  episodes: {
    type: Number,
    label: "Number of episodes",
    min: 0
  },
  type: {
    type: String,
    allowedValues: ["TV", "OVA", "Movie", "Special"],
    label: "Type of the show"
  },
  genres: {
    type: [String]
  },
  "genres.$": {
    type: String,
    allowedValues: ["Action", "Adventure", "Cars", "Comedy", "Dementia", "Demons", "Drama", "Ecchi", "Fantasy", "Game", "Harem", "Hentai", "Historical", "Horror", "Josei", "Kids", "Magic", "Martial Arts", "Mecha",
                    "Military", "Music", "Mystery", "Parody", "Police", "Psychological", "Romance", "Samurai", "School", "Sci-Fi", "Seinen", "Shoujo", "Shoujo Ai", "Shounen", "Shounen Ai", "Slice of Life", "Space", "Sports", "Super Power",
                    "Supernatural", "Thriller", "Vampire", "Yaoi", "Yuri"]
  },
  coverUrl: {
    type: String,
    label: "The url of the cover image",
    regEx: SimpleSchema.RegEx.Url
  },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date;
      } else if (this.isUpsert) {
        return {
          $setOnInsert: new Date
        };
      } else {
        this.unset();
      }
    }
  }
});

Animes.attachSchema(Schemas.Anime);
